#!/bin/sh

install_npm_linux() {
  echo "Installing npm:"
  if ! which npm &>/dev/null ; then
    if which apt-get &>/dev/null ; then
      apt-get install nodejs
    elif which emerge &>/dev/null ; then
      emerge nodejs
    elif which pacman &>/dev/null ; then
      pacman -S nodejs npm
    elif which pkg &>/dev/null ; then
      pkg install node
    elif which yum &>/dev/null ; then
      yum install nodejs
    else
      echo "  ERROR: Package manager not found"
      exit 2
    fi
  else
    echo "  npm already installed"
  fi

  return 0
}

install_npm_macos() {
  echo "Installing npm:"
  if ! which npm &>/dev/null ; then
    local node_version="${VERSION:-$(wget -qO- https://nodejs.org/dist/latest/ | sed -nE 's|.*>node-(.*)\.pkg</a>.*|\1|p')}.pkg"

    curl "https://nodejs.org/dist/latest/node-$node_version" > "$HOME/Downloads/node-latest.pkg" && \
    sudo installer -store -pkg "$HOME/Downloads/node-latest.pkg" -target "/"
  else
    echo "  npm already installed"
  fi

  return 0
}

install_npm_packages() {
  echo "Installing npm packages"
  local packages=("node-red")

  for package in ${packages[@]} ; do
    if ! npm list -g $package &>/dev/null ; then
      echo "  Installing $package"
      if test $1 = "sudo" ; then
        sudo npm install -g --unsafe-perm $package
      else
        npm install -g $package
      fi
    else
      echo "  $package already installed"
    fi
  done

  return 0
}

install_novnc() {
  echo "Downloading noVNC"
  if [[ ! -d "noVNC-master" ]] ; then
    curl https://codeload.github.com/novnc/noVNC/zip/master -o novnc.zip
    unzip novnc.zip
  else
    echo "  noVNC already present"
  fi
}

main() {
  case $OSTYPE in
    linux-gnu)
      install_npm_linux && \
      install_npm_packages sudo && \
      install_novnc ;;
    darwin*)
      install_npm_macos && \
      install_npm_packages sudo && \
      install_novnc ;;
    cygwin|msys|win32)
      install_npm_packages sudo && \
      install_novnc ;;
    *)
      echo "ERROR: Unknown OS type '$OSTYPE'"
      exit 1
      ;;
  esac

  return 0
}

main
