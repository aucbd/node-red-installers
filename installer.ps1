function Check_Program_Installed( $programName )
{
  $x86_check = ((Get-ChildItem "HKLM:SoftwareMicrosoftWindowsCurrentVersionUninstall") |
  Where-Object { $_."Name" -like "*$programName*" } ).Length -gt 0;

  if(Test-Path 'HKLM:SoftwareWow6432NodeMicrosoftWindowsCurrentVersionUninstall') {
    $x64_check = ((Get-ChildItem "HKLM:SoftwareWow6432NodeMicrosoftWindowsCurrentVersionUninstall") |
    Where-Object { $_."Name" -like "*$programName*" } ).Length -gt 0;
  }

  return $x86_check -or $x64_check;
}

if (!Check_Program_Installed("node")) {
  Write-Host "Install Node.js and npm with provided binaries then run script again"
  Start-Process "https://nodejs.org/en/download/"
} else {
  Write-Host "Installing Node-RED"
  npm install -g node-red
}

Write-Host "Downloading noVNC"
if (!(Test-Path -Path "noVNC-master")) {
  Invoke-WebRequest -Uri "https://codeload.github.com/novnc/noVNC/zip/master" -OutFile "novnc.zip"
  Expand-Archive "novnc.zip"
} else {
  Write-Host "  noVNC already present"
}

Read-Host -Prompt "Press Enter to close."
