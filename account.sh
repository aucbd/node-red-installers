#!/bin/sh


account_create_linux() {
  local user_new=""

  while test -z "$user_new" ; do
    echo "Enter a username: "
    read user_new
    [[ "$user_new" =~ .*[^A-Za-z0-9_].* ]] && echo "Invalid username, use A-Z a-z 0-9 _" && user_new=""
    echo
  done

  sudo useradd -m "$user_new"
  sudo usermod -a -G dialout "$user_new"

  echo "Insert a password for the new user:"
  sudo passwd "$user_new"

  echo "To switch to the new user use the command: su - $user_new"

  return 0
}


account_create_macos() {
  local user_new=""

  while test -z "$user_new" ; do
    echo "Enter a username: "
    read user_new
    [[ "$user_new" =~ .*[^A-Za-z0-9_].* ]] && echo "Invalid username, use A-Z a-z 0-9 _" && user_new=""
    echo
  done

  sudo dscl / -create /Users/"$user_new"
  sudo dscl / -create /Users/"$user_new" RealName "$user_new"
  sudo dscl / -create /Users/"$user_new" UserShell /bin/bash
  sudo dscl / -create /Users/"$user_new" UniqueID "999"
  sudo dscl / -create /Users/"$user_new" PrimaryGroupID 1000
  sudo dscl / -create /Users/"$user_new" NFSHomeDirectory /Users/"$user_new"
  sudo dscl / -append /Groups/admin GroupMembership "$user_new"

  echo "Insert a password for the new user:"
  sudo passwd "$user_new"

  echo "To switch to the new user use the command: su - $user_new"
  echo "To see the new account reboot the machine"

  return 0
}


main() {
  case $OSTYPE in
    linux-gnu)
      account_create_linux
      ;;
    darwin*)
      account_create_macos
      ;;
    cygwin|msys|win32)
      echo "Account creation not supported on Windows Subsystem for Linux"
      exit 1
      ;;
    *)
      echo "ERROR: Unknown OS type '$OSTYPE'"
      exit 1
      ;;
  esac

  return 0
}

main
